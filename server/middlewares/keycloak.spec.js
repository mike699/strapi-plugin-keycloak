const getLoginStatusAndProfile = require("../utils/get-login-status-and-profile");

jest.mock("../utils/get-login-status-and-profile", () => {
  return jest.fn();
});

const middleware = require("./keycloak");

describe("Keycloak middleware", () => {
  let ctx;
  let nextFunction;

  beforeEach(() => {
    global.strapi = { config: { keycloak: {} } };
    ctx = {};
    nextFunction = jest.fn();
    getLoginStatusAndProfile.mockResolvedValue({
      isLoggedIn: false,
    });
  });

  it("should return a 403 code when no user is logged in", async () => {
    // test
    await middleware()(ctx, nextFunction);

    // assert
    expect(nextFunction).not.toHaveBeenCalled();
    expect(ctx.status).toEqual(403);
  });

  it("should call the next function when a user is logged in", async () => {
    // setup
    getLoginStatusAndProfile.mockResolvedValue({
      isLoggedIn: true,
    });

    // test
    await middleware()(ctx, nextFunction);

    // assert
    expect(nextFunction).toHaveBeenCalled();
  });

  it("should populate the context with the user profile when a user is logged in", async () => {
    // setup
    const profile = {
      username: "Max Mustermann",
    };
    getLoginStatusAndProfile.mockResolvedValue({
      isLoggedIn: true,
      profile,
    });

    // test
    await middleware()(ctx, nextFunction);

    // assert
    expect(ctx.state.keycloak.profile).toEqual(profile);
  });
});
