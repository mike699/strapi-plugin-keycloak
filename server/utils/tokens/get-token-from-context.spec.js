const { getTokenFromContext } = require("./get-token-from-context");

describe("getTokenFromContext", () => {
  beforeEach(() => {
    global.strapi = {
      config: { keycloak: { pluginJwtSecret: "pluginJwtSecret" } },
    };
  });

  it("should return undefined if no access token is set in the context", () => {
    expect(getTokenFromContext({})).toBeUndefined();
  });

  it("should return the access token from the session if one is set", () => {
    expect(
      getTokenFromContext({
        session: { keycloak: { accessToken: "abc" } },
      })
    ).toEqual("abc");
  });
});
